package com.example.ti.myapplication;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Date;

/**
 * Programa exemplo para os comandos, recomenda-se acompanhar fluxo de dados pelo
 * LogCat para melhor entendimento
 *
 * Este é um APP apenas para entendimento das rotinas, o programador deve implementar
 * as requisições de modo assincrono com uso de classes adequadas
 *
 */
public class MainActivity extends Activity {

    public static final String TAG = "MainActivity";

    private static final int REQUEST_SELECT_DEVICE_1 = 1;

    private static final int REQUISITA_PERMISSAO_COARSE_LOCATION = 1;
    private static final int REQUISITA_PERMISSAO_WRITE_EXTERNAL = 2;
    private static final int REQUISITA_PERMISSAO_FINE_LOCATION = 3;

    private ComunicaoIndicadorBLE comunicaoIndicadorBLE;

    private TextView textMAC;

    public EditText editCMD;
    public EditText editCMD2;

    // Todo: para testes
   // private int autoVerifica = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requisitaPermissaoEmRunTime();

        editCMD = findViewById(R.id.editCMD);
        editCMD2 = findViewById(R.id.editCMD2);


        Button butnListar = findViewById(R.id.butnListar);
        Button butnSobeServico = findViewById(R.id.butnSobeServico);
        Button butnConectar = findViewById(R.id.butnConectar);
        Button butnDesconectar = findViewById(R.id.butnDesconectar);

        Button butnCMD = findViewById(R.id.butnCMD);
        Button butnCMD2 = findViewById(R.id.butnCMD2);

        textMAC = findViewById(R.id.textMAC);


        butnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(MainActivity.this, DeviceListActivity.class);
                startActivityForResult(newIntent, REQUEST_SELECT_DEVICE_1);
            }
        });


        butnSobeServico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comunicaoIndicadorBLE = new ComunicaoIndicadorBLE(getBaseContext());
                comunicaoIndicadorBLE.SobeService();
            }
        });

        butnConectar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comunicaoIndicadorBLE.ConectarServico(textMAC.getText().toString());
            }
        });


        butnDesconectar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (comunicaoIndicadorBLE != null) {
                    comunicaoIndicadorBLE.DesconectarServico();
                }
            }
        });

        butnCMD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comunicaoIndicadorBLE.enviaCMD(editCMD.getText().toString());
            }
        });

        butnCMD2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comunicaoIndicadorBLE.enviaCMD(editCMD2.getText().toString());
            }
        });
    }

    private void requisitaPermissaoEmRunTime() {
        // versão 6 em diante requer solicitação de permissão em runtime, mesmo presente no manifest
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Android M checar permissao em runtime
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Requer ACCESS_COARSE_LOCATION");
                // Este APP requer permissão de localização para beacons.
                builder.setMessage("Este APP requer permissão de localização para IoTs");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            // call back desse método implementado -> onRequestPermissionsResult
                            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUISITA_PERMISSAO_COARSE_LOCATION);
                        }
                    }
                });
                builder.show();
            }

            if (this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Requer ACCESS_FINE_LOCATION");
                // Este APP requer permissão de uso do GPS.
                builder.setMessage("Este APP requer permissão de uso do GPS");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            // call back desse método implementado -> onRequestPermissionsResult
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUISITA_PERMISSAO_FINE_LOCATION);
                        }
                    }
                });
                builder.show();
            }

            if (this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Requer WRITE_EXTERNAL_STORAGE");
                // Este APP requer permissão de escrita no SD CARD
                builder.setMessage("Este APP requer permissão de escrita no SD CARD");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            // call back desse método implementado -> onRequestPermissionsResult
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUISITA_PERMISSAO_WRITE_EXTERNAL);
                        }
                    }
                });
                builder.show();
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_SELECT_DEVICE_1) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                String deviceAddress = data.getStringExtra(BluetoothDevice.EXTRA_DEVICE);
                textMAC.setText(deviceAddress);
            }
        }
    }

//    private void runThread() {
//        new Thread() {
//            boolean rodar = true;
//            int i = 1;
//
//            public void run() {
//                while (rodar) {
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            if (comunicaoIndicadorBLE.isGattConectado()) {
//                                comunicaoIndicadorBLE.enviaCMD(null);
//                                txtPeso.setText(comunicaoIndicadorBLE.getPesoLido());
//                                txtStatus.setText(comunicaoIndicadorBLE.getPesoStatus());
//                                txtBateria.setText(comunicaoIndicadorBLE.getBateriaNivel());
//                                // https://www.journaldev.com/17850/java-string-format
//                                butnPeso.setText("Pesagem => " + String.format("{%,d}", i));
//
//                                autoVerifica = autoVerifica + 1;
//                                if (autoVerifica == 60) {
//                                    comunicaoIndicadorBLE.pedeVerifica();
//                                    autoVerifica = 0;
//                                }
//                            } else {
//                                Date agora = new Date();
//                                txtPeso.setText("***ACTION_GATT_DISCONNECTED***");
//                                txtStatus.setText(agora.toString());
//                                rodar = false;
//                            }
//                            Log.i(TAG, comunicaoIndicadorBLE.toString());
//                        }
//
//                        ;
//                    });
//                    ++i;
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }.start();
//    }
}
