package com.example.ti.myapplication;


import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.util.Date;

public class ComunicaoIndicadorBLE {

    public static final String TAG = "ComunicaoIndicadorBLE";

    private Context context;
    private UartService uartService = null;

    // retorno recebido do serviço da Uart
    private static final String retornoDeviceConectadoNoUart = "Connected!";

    // retornos da bateria
    private static final String retornoBateria1Baixa = "batt;low;1;";
    private static final String retornoBateria1Alta = "batt;hight;1;";

    // retorno do verifica
    private static final String  retornoVerifica1OK = ";verif;ok;1;";
    // retorno do tara
    private static final String  retornoTara1OK = ";tara;ok;1;";

    private boolean GattConectado = false;

    private boolean bTara;
    private boolean bVerifica;
    private boolean bBateria;

    // hora quando recebeu um OK de verifica
    private Date verifica1As;

    private Date tara1As;

    private Date bateria1As;


    // envia um comando para iniciar a rede mesh entre indicadores
    private static final String cmdPeso = ";peso";
    private static final String cmdVerifica = ";verif";
    private static final String cmdTara = ";tara";
    private static final String cmdBateria = ";batt";

    private String pesoLido = "0";
    private float pesoLido1 = 0;
    private String pesoStatus = "";
    private String bateriaNivel1 = null;

    public ComunicaoIndicadorBLE(Context context) {
        Log.i(TAG, "ServicoIndicadorBLE()");
        this.context = context;
    }

    public void pedeVerifica() {
        bVerifica = true;
    }

    public void pedeTara() {
        bTara = true;
    }

    public void pedeBateria() {
        bBateria = true;
    }

    public void SobeService() {
        // Cria um serviço UartService no context
        Intent bindIntent = new Intent(context, UartService.class);
        // Connect to an application service
        context.bindService(bindIntent, ServicoDeConexaoComUART, Context.BIND_AUTO_CREATE);
        // registra o broadcast receiver considerando o filtro
        LocalBroadcastManager.getInstance(context).registerReceiver(broadcastReceiver, intentFiltroUART());
    }

    public boolean ConectarServico(String mac) {
        return uartService.connect(mac);
    }

    public void DesconectarServico() {
        GattConectado = false;
        if (uartService != null) {
            uartService.disconnect();
            context.unbindService(ServicoDeConexaoComUART);
            uartService.stopSelf();
            uartService = null;
            try {
                LocalBroadcastManager.getInstance(context).unregisterReceiver(broadcastReceiver);
            } catch (Exception ignore) {
                Log.e(TAG, ignore.toString());
            }
        }
    }


    public void enviaCMD(String cmdAvulso) {

        if (GattConectado) {
            if (cmdAvulso != null) {
                Korth_UART_TX(cmdAvulso);
            }
            else
            {
                // ação padrão
                if (!bTara && !bVerifica && !bBateria) {
                    Korth_UART_TX(cmdPeso);
                }

                if (bTara) {
                    bTara = false;
                    Korth_UART_TX(cmdTara);
                }

                if (bVerifica) {
                    bVerifica = false;
                    Korth_UART_TX(cmdVerifica);
                }

                if (bBateria) {
                    bBateria = false;
                    Korth_UART_TX(cmdBateria);
                }
            }
        }
    }

    public String getPesoLido() {
        return pesoLido;
    }

    public String getPesoStatus() {
        return pesoStatus;
    }


    public boolean isGattConectado() {
        return GattConectado;
    }

    /**
     * https://developer.android.com/reference/android/content/ServiceConnection
     * Interface for monitoring the state of an application service
     */
    private ServiceConnection ServicoDeConexaoComUART = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            uartService = ((UartService.LocalBinder) rawBinder).getService();
            // exemplo de saida -> com.example.ti.myapplication.UartService@21a78ef8
            Log.i(TAG, "Iniciar serviço UART : " + uartService);
            if (!uartService.initialize()) {
                Log.e(TAG, "Falhou ao iniciar o Bluetooth");
            }
        }

        public void onServiceDisconnected(ComponentName classname) {
            uartService = null;
        }
    };


    private static IntentFilter intentFiltroUART() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(UartService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(UartService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(UartService.DEVICE_DOES_NOT_SUPPORT_UART);
        return intentFilter;
    }

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        public void onReceive(final Context context, Intent intent) {
            /*
              https://developer.android.com/reference/android/content/Intent.html#getAction()
              Retrieve the general action to be performed, such as ACTION_VIEW. The action describes the general
              way the rest of the information in the intent should be interpreted -- most importantly,
              what to do with the data returned by getData().
             */
            String action = intent.getAction();

            if (action != null) {
                Log.i(TAG, action.toString());

                if (action.equals(UartService.ACTION_GATT_CONNECTED)) {
                    GattConectado = true;
                }

                if (action.equals(UartService.ACTION_GATT_DISCONNECTED)) {
                    GattConectado = false;
                }

                if (action.equals(UartService.ACTION_GATT_SERVICES_DISCOVERED)) {
                    uartService.enableTXNotification();
                }

                if (action.equals(UartService.ACTION_DATA_AVAILABLE)) {
                    final byte[] txValue = intent.getByteArrayExtra(UartService.EXTRA_DATA);
                    try {
                        String text = new String(txValue, "UTF-8");
                        recebeDado(text);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (action.equals(UartService.DEVICE_DOES_NOT_SUPPORT_UART)) {
                uartService.disconnect();
            }
        }
    };

    private void recebeDado(String dado) {

        dado = dado.replaceAll("(\\r|\\n)", "");
        Log.i(TAG,UartService.ACTION_DATA_AVAILABLE + " -> " + dado.toString());

        if (dado.contains(retornoVerifica1OK)) {
            verifica1As = new Date();
        }

        if (dado.contains(retornoTara1OK)) {
            tara1As = new Date();
        }


        if (dado.contains(retornoBateria1Alta) || dado.contains(retornoBateria1Baixa)) {
            bateria1As = new Date();
            bateriaNivel1 = dado;
        }

        if (dado.equals(retornoDeviceConectadoNoUart)) {
            Log.i(TAG,"UART avisou que conectou no service");
        }


        // checar se string é peso potencial
        // começar com "+" ou - e conter 3 partes (sinal com peso;status;"lixo")
        if ((dado.startsWith("+") || dado.startsWith("-")) && dado.endsWith(";")) {
            String[] parts = dado.split(";");
            if (parts.length == 2) {
                float rA = 0;
                try {
                    rA = Float.parseFloat(parts[0]);
                } catch (NumberFormatException e) {
                    rA = 0;
                    Log.e(TAG, "Erro no parse do peso NumberFormatException");
                }

                pesoLido1 = rA;
                pesoStatus = parts[1];
                pesoLido = String.valueOf(rA);
            }
        }

        // Log.i(TAG,toString());
    }

    private void Korth_UART_TX(String comando) {
        // Broadcast receiver recebe de modo assincrono retorno do TX
        if (uartService != null) {
            Log.i(TAG,"Korth_UART_TX(" + comando + ")");
            uartService.writeRXCharacteristic(comando.getBytes());
        } else {
            Log.i(TAG, "Não conectado");
        }
    }

    @Override
    public String toString() {
        return "ComunicaoIndicadorBLE{" +
                "GattConectado=" + GattConectado +
                ", verifica1As=" + verifica1As +
                ", tara1As=" + tara1As +
                ", bateria1As=" + bateria1As +
                ", pesoLido1=" + pesoLido1 +
                ", pesoStatus='" + pesoStatus +
                ", bateriaNivel1='" + bateriaNivel1  +
                '}';
    }

    public String getBateriaNivel() {
        return bateriaNivel1;
    }

}

